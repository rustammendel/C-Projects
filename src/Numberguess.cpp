//============================================================================
// Name        : Numberguess.cpp
// Author      : Rustam Mendel
// Version     : v2
// Copyright   : This is my project :)
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include <cstdlib>

using namespace std;

int main()

{

	int num;
	int guess;
	int gm;
	bool problem;
	string tmp;

	cout << "Guess The Number " << endl;
	cout << "select game mode \n 1-singleplayer \n 2-multiplayer \n";
	cin >> gm;
	if (gm == 1) {
		srand(time(0));
		num = rand() % 100 + 1;

		do {

			cout << "Enter a guess between 1 and 100 : ";

			cin >> guess;

			problem = cin.fail();

			cin.clear();
			getline(cin, tmp, '\n');

			if (guess < 100 && guess > 0) {
				if (guess > num) {

					cout << "High value! " << endl;
					;
				} else if (guess < num) {

					cout << "Low value! " << endl;
					;
				} else if (num == guess) {

					cout << "Correct!  ";
				}
			} else if (problem) {
				cout << "Wrong input! Input a number please" << endl;
			} else {

				cout << "Wrong input" << endl;

			}

		} while (guess != num);
	} else if (gm == 2) {

		do {
			cout << "First Player- Please enter a number ";

			cin >> num;
			problem = cin.fail() || num < 0 || num > 100;
			cin.clear();
			getline(cin, tmp, '\n');
			if (problem) {
				cout << "Wrong input \n";
			}
		} while (problem);
		cout << string(100, '\n');
		do {

			cout << "Enter a guess between 1 and 100 : ";

			cin >> guess;

			problem = cin.fail();

			cin.clear();
			getline(cin, tmp, '\n');

			if (guess < 100 && guess > 0) {
				if (guess > num) {

					cout << "High value! " << endl;
					;
				} else if (guess < num) {

					cout << "Low value! " << endl;
					;
				} else if (num == guess) {

					cout << "Correct!  ";
				}

				else if (problem) {
					cout << "Wrong input! Input a number please" << endl;
				}
			} else {

				cout << "Wrong input" << endl;

			}

		} while (guess != num);
	}
	return 0;

}
